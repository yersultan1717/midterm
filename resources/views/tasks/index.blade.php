@extends('layout')

@section('content')
    <div class="container">
        <h3>MidTerm</h3>


        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <table class="table">
                    <thead>
                        <tr>

                            <td>Rating</td>

                        </tr>
                    </thead>
                    <tbody>
                    @foreach($tasks as $task)
                    <tr>

                        <td>{{$task->title}}</td>
                        <td>
                            <a href="{{ route('tasks.show', $task->id) }}">
                                <i>show feedback</i>
                            </a>


                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <a href="{{ route('tasks.create') }}" class="btn">give feedback</a>
    </div>
@endsection